package buildbot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
)

var (
	buildbotUrl    = "https://buildbot.pkgbuild.com/"
	buildbotLogin  = "https://buildbot.pkgbuild.com/auth/login"
	buildbotBuilds = "https://buildbot.pkgbuild.com/api/v2/builds"
)

func getTwistedSession(cookies []*http.Cookie) (string, error) {
	for _, cookie := range cookies {
		if cookie.Name == "TWISTED_SESSION" {
			return cookie.Value, nil
		}
	}
	return "", fmt.Errorf("failed to find cookie")
}

func GetSessionToken(token string) (string, error) {
	jar, _ := cookiejar.New(nil)
	client := &http.Client{Jar: jar}
	if _, err := client.Head(buildbotUrl); err != nil {
		return "", err
	}
	f := url.Values{"token": {token}}
	if _, err := client.PostForm(buildbotLogin, f); err != nil {
		return "", err
	}
	u, _ := url.Parse(buildbotUrl)
	token, err := getTwistedSession(client.Jar.Cookies(u))
	if err != nil {
		return "", err
	}
	return token, nil
}

type Properties struct {
	Architecture []string `json:"architecture"`
	Owner        []string `json:"owner"`
	Buildername  []string `json:"buildername"`
	Owners       []string `json:"owners"`
	Package      []string `json:"package"`
	Project      []string `json:"project"`
	Pkgbase      []string `json:"pkgbase"`
}

func (p Properties) GetOwner() string {
	if len(p.Owner) == 0 {
		return ""
	}
	return p.Owner[0]
}

type Build struct {
	Buildid     int        `json:"buildid"`
	Builderid   int        `json:"builderid"`
	CompletedAt int64      `json:"completed_at"`
	StartedAt   int64      `json:"started_at"`
	StateString string     `json:"state_string"`
	Complete    bool       `json:"complete"`
	Properties  Properties `json:"properties"`
	Results     int        `json:"results"`
}

type Builds struct {
	Builds []Build `json:"builds"`
}

func GetBuilds(token string) ([]Build, error) {
	var builds Builds
	var ret []Build
	client := &http.Client{}
	url := "https://buildbot.pkgbuild.com/api/v2/builds?property=*"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", `application/json`)
	req.AddCookie(&http.Cookie{Name: "TWISTED_SESSION", Value: token})
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	d := json.NewDecoder(resp.Body)
	d.Decode(&builds)
	for _, i := range builds.Builds {
		if strings.Contains(i.Properties.GetOwner(), "foxboron") {
			if len(i.Properties.Pkgbase) == 0 {
				continue
			}
			ret = append(ret, i)
		}
	}
	return ret, nil
}

type SchedRequest struct {
	Method  string            `json:"method"`
	Id      int               `json:"id"`
	Jsonrpc string            `json:"jsonrpc"`
	Token   string            `json:"token"`
	Params  map[string]string `json:"params"`
}

func NewSchedRequest() *SchedRequest {
	return &SchedRequest{
		Id:      1,
		Method:  "force",
		Jsonrpc: "2.0",
		Params:  make(map[string]string),
	}
}

// {"id":1,"jsonrpc":"2.0","method":"force","params":{"builderid":"1","username":"","reason":"force build","branch":"","project":"","repository":"","revision":"","build_repository":"extra","architecture":"x86_64","package":"delve","to_repository":"","should_reproduce":false,"bump_pkgrel":false,"setconf_set_pkgver":"","setconf_set_pkgrel":"","setconf_set_commit":"","setconf_set_tag":""}}

func ScheduleBuild(token string) (*Build, error) {
	client := &http.Client{}
	forceBuild := "https://buildbot.pkgbuild.com/api/v2/forceschedulers/build-package"
	r := NewSchedRequest()

	b, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", forceBuild, bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Accept", `application/json`)
	req.AddCookie(&http.Cookie{Name: "TWISTED_SESSION", Value: token, Path: "/"})
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	b, _ = io.ReadAll(resp.Body)
	return nil, nil
}
