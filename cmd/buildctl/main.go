package main

import (
	"errors"
	"log"

	"github.com/spf13/cobra"
)

type cliCommand struct {
	Cmd *cobra.Command
}

var (
	CliCommands = []cliCommand{}
	ErrSilent   = errors.New("SilentErr")
	rootCmd     = &cobra.Command{
		Use:           "buildctl",
		Short:         "Arch Linux build manager",
		SilenceUsage:  true,
		SilenceErrors: true,
	}
)

func main() {
	for _, cmd := range CliCommands {
		rootCmd.AddCommand(cmd.Cmd)
	}

	rootCmd.SetFlagErrorFunc(func(cmd *cobra.Command, err error) error {
		cmd.Println(err)
		cmd.Println(cmd.UsageString())
		return ErrSilent
	})

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
