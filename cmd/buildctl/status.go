package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.archlinux.org/foxboron/buildctl/buildbot"
	"gitlab.archlinux.org/foxboron/buildctl/config"
)

var status = &cobra.Command{
	Use:   "status",
	Short: "build statuses",
	RunE:  statusCmd,
}

func statusCmd(cmd *cobra.Command, args []string) error {
	conf, err := config.GetCacheConfig()
	if err != nil {
		return err
	}
	builds, err := buildbot.GetBuilds(conf.SessionToken)
	if err != nil {
		return err
	}

	fmt.Println("Ongoing Builds")
	for _, i := range builds {
		if !i.Complete {
			fmt.Printf("\t%s\n", i.Properties.Pkgbase[0])
		}
	}
	fmt.Println()

	fmt.Println("Finished Builds")
	for _, i := range builds {
		if i.Results == 0 && i.Complete {
			fmt.Printf("\t✓ %s\n", i.Properties.Pkgbase[0])
		}
	}

	fmt.Println()
	fmt.Println("Failed Builds")
	for _, i := range builds {
		if i.Results != 0 {
			fmt.Printf("\t✗ %s\n", i.Properties.Pkgbase[0])
		}
	}
	fmt.Println()
	return nil
}

func init() {
	CliCommands = append(CliCommands, cliCommand{
		Cmd: status,
	})
}
