all: build
build: buildctl

.PHONY: buildctl
buildctl:
	go build -o $@ ./cmd/$@
